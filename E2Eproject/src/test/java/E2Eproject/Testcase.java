package E2Eproject;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import pageObject.Landingpage;
import pageObject.LoginPage;



public class Testcase extends Base {
	WebDriver driver;
	
	@Test
	
	public void basepagenavigation() throws IOException {
		driver=initializeDriver();
		driver.get("https://qaclickacademy.com");
		
		//calling getlogin method
		Landingpage l=new Landingpage(driver);
		//l.closepopup();
		l.getlogin();
		
		LoginPage l1=new LoginPage(driver);
		l1.Page();
	}

}
